# Introduction to UNIX

## Formation Labo

frederic.boyer@univ-grenoble-alpes.fr

---

## Interacting with a UNIX computer


### The command shell endless loop

![Interactive loop](./PRACTICAL1-UNIX/images/loop.svg) <!-- .element: width="80%"-->

--

### Bash

The basic command interpreter on the machine (and most often found on a linux machine) is `bash`.


> Bash is a command processor that typically runs in a text window, where the user types commands that cause actions. Bash can also read commands from a file, called a script. Like all Unix shells, it supports filename globbing (wildcard matching), piping, here documents, command substitution, variables and control structures for condition-testing and iteration. The keywords, syntax and other basic features of the language were all copied from sh. Other features, e.g., history, were copied from csh and ksh. Bash is a POSIX shell, but with a number of extensions.
> 
> [wikipedia:bash](https://en.wikipedia.org/wiki/Bash_%28Unix_shell%29)

--

### RTFM !

> RTFM is an initialism for the expression "read the fucking manual" or, in the context of the Unix computer operating system, "read the fucking man page". The RTFM comment is usually expressed when the speaker is irritated by another person's question or lack of knowledge. It refers to either that person's inability to read a technical manual, or to their perceived laziness in not doing so first, before asking the question.
>
> [wikipedia:RTFM](https://en.wikipedia.org/wiki/RTFM)


The official `bash` documentation: [documentation]( https://www.gnu.org/software/bash/manual/ )

--

### RTFM: getting help with `man`

> A man page (short for manual page) is a form of software documentation usually found on a Unix or Unix-like operating system. Topics covered include computer programs (including library and system calls), formal standards and conventions, and even abstract concepts. A user may invoke a man page by issuing the man command.
>
> [wikipedia:man page](https://en.wikipedia.org/wiki/Man_page)

| Useful command       | What it does                            |
|----------------------|-----------------------------------------|
| `man <command>`      | prints manual for the `command` command |

For example, to get the manual page for the `man` command:

```bash
man man
```

--

### RTFM: getting help with `-h` or `--help`

When there is no man page associated to a command one can use the `-h` or `--help` options:

For example, to get the help associated to the `man` command:

```bash
man --help
```

---

## Filesystem basic commands and streams

![Système de fichier](./PRACTICAL1-UNIX/images/filesystem.png) <!-- .element: width="40%"-->

--

### Absolute path

> An absolute or full path points to the same location in a file system, regardless of the current working directory. To do that, it must include the root directory.
>
> [wikipedia:Path(computing)](https://en.wikipedia.org/wiki/Path_(computing))

The root of the filesystem is designed by `/`.

The different part of the path are separated by `/`.

--

### Absolute path


![Système de fichier](./PRACTICAL1-UNIX/images/filesystem.png) <!-- .element: width="30%"-->

The red path is : `/etc/passwd`

--

### Relative path

> By contrast, a relative path starts from some given working directory, avoiding the need to provide the full absolute path.
>
> [wikipedia:Path (computing)]( https://en.wikipedia.org/wiki/Path_(computing) )

Special directories :
- `~` : *home directory* for the current user
- `~name` : *home directory* for user *name*
- `.` : Current directory
- `..` : Parent directory

![Special directories](./PRACTICAL1-UNIX/images/filesystem-special.png) <!-- .element: width="30%"-->


--

### A list of useful commands to interact with the filesystem

| Useful commands            | What it does                |
|----------------------------|-----------------------------|
| `pwd`                      | print working directory     |
| `cd <directory>`           | change directory            |
| `mkdir <filename>`         | create directory            |
| `ls <filename>`            | list files and directories  |
| `touch <filename>`         | create or touch a file      |
| `cp <filename> <filename>` | copy files or directories   |
| `mv <filename> <filename>` | move files or directories   |
| `rm <filename>`            | remove files or directories |


--

### Permissions

Files and directories are assigned permissions or access rights to specific users and groups of users. The permissions control the ability of the users to view, change, navigate, and execute the contents of the file system.

> Permissions on Unix-like systems are managed in three distinct scopes or classes. 
> These scopes are known as user, group, and others.
>
> [wikipedia:unix permissions](https://en.wikipedia.org/wiki/File_system_permissions#Traditional_Unix_permissions)

--

### Permissions

 ![Permissions](./PRACTICAL1-UNIX/images/permissions.png) <!-- .element: width="20%"-->

> The modes indicate which permissions are to be granted or taken away from the specified classes. 
> There are three basic modes which correspond to the basic permissions:
> 
> | Mode | Name	   | Description                                |
> |------|---------|--------------------------------------------|
> |  r	 | read	   | read a file or list a directory's contents |
> |  w	 | write   | write to a file or directory               |
> |  x	 | execute | execute a file or recurse a directory tree |
>
> [wikipedia:modes](https://en.wikipedia.org/wiki/Modes_(Unix))

--

### View and change permissions

| Useful commands              | What it does       |
|------------------------------|--------------------|
| `ls -l`                      | view permissions   |
| `chmod <options> <filename>` | change permissions |


For exemple:

```bash
fboyer@leca-34:~/Documents/COURS/FORMATION INTERNE$ ls -l index.html
-rw-rw-r-- 1 fboyer fboyer 3101 déc. 21 17:09 index.html
fboyer@leca-34:~/Documents/COURS/FORMATION INTERNE$ chmod 500 index.html
fboyer@leca-34:~/Documents/COURS/FORMATION INTERNE$ ls -l index.html
-r-x------ 1 fboyer fboyer 3101 déc. 21 17:09 index.html
```

--

## Commands to work with text

### Visualize the content of a file

| Useful commands    | What it does                           |
|--------------------|----------------------------------------|
| `less <filename>`  | utility to explore text files          |


#### The `less` command

| shortcut              | What it does                              |
|-----------------------|-------------------------------------------|
| `h`                   | display help                              |
| `q`                   | quit                                      |
| `/`                   | search for a regular pattern              |
| `n`                   | for the Next occurence of the pattern     |
| `shift-n`             | for the previous occurence of the pattern |
| `arrows` and `space`  | to navigate                               |
| `g` and `G`           | go to top and end of the file             |

--

### Edit a text file

| Useful commands    | What it does       |
|--------------------|--------------------|
| `nano <filename>`  | Simple text editor |


![Système de fichier](./PRACTICAL1-UNIX/images/nano.png) <!-- .element: width="100%"-->

--

### Basic `Unix` commands to manipulate text files

| Useful commands               | What it does                                                     |
|-------------------------------|------------------------------------------------------------------|
| `cat <filename>`              | output the content of `filename` file                            |
| `head [-<N>] <filename>`      | output the first `N` lines of `filename`                         |
| `tail [-<N>] <filename>`      | output the last `N` lines of `filename`                          |
| `sort [options] <filename>`   | sort the content of `filename` and output it                     |
| `cut [options] <filename>`    | extract some column from `filename` and output it                |
| `diff [options] <filenames>`  | compare files line by line and output the differences            |

--

| Useful commands               | What it does                                                     |
|-------------------------------|------------------------------------------------------------------|
| `join [options] <filename> <filename>` | join files on the basis of column content               |
| `paste [options] <filenames>` | paste files line by line                                         |
| `wc [options] <filenames>`    | count characters, words or lines                                 |
| `find <directory> [options]`  | search files (Ex: `find . -name '*.txt'`)                        |
| `sed <command> <filename>`    | process file line by line for basic editing                      |
| `grep [options] <regular expression> <filenames>` | search files for a *pattern*                 |
| `egrep [options] <extended regular expression> <filenames>` | similar as using the `-e` option of `grep` |

--

### `Unix` streams

> In computer programming, standard streams are preconnected input and output communication channels between a computer program and its environment when it begins execution. 
> The three I/O connections are called standard input (`stdin`), standard output (`stdout`) and standard error (`stderr`).
>
A basic `Unix` command: Piping a stream into another command> [wikipedia:streams](https://en.wikipedia.org/wiki/Standard_streams)

--

### A basic `Unix` command

![Système de fichier](./PRACTICAL1-UNIX/images/command.svg) <!-- .element: width="50%"-->

standard input (`stdin`), standard output (`stdout`), standard error (`stderr`) and parameters don't need to be specified.

By default, `stdin` is empty, `stdout` and `stderr` output their content to the terminal.
--

### A basic `Unix` command: Specifying parameters

#### Exemples: Parameters

```bash
grep -B 2 root /etc/passwd
```

![Command example](./PRACTICAL1-UNIX/images/command-grep1.svg) <!-- .element: width="60%"-->

--

### A basic `Unix` command: Sending the content of a text file to the standard input

Most of the commands that handle text can, if no file is given as a parameter, use the content of `stdin`.

#### Exemples: Redirecting the standard input

```bash
grep -B 2 root < /etc/passwd
```

![Redirect stdin](./PRACTICAL1-UNIX/images/command-grep2.svg) <!-- .element: width="40%"-->

--

### A basic `Unix` command: Creating a text file from the output of a command

#### Exemples: Redirecting the standard output

```bash
# > create or replace file
# >> append to file
grep -B 2 root < /etc/passwd > result
```

![Redirect stdout](./PRACTICAL1-UNIX/images/command-grep3.svg) <!-- .element: width="40%"-->

--

### A basic `Unix` command: Piping a stream into another command

#### Exemples: Redirecting the standard output

```bash
# > create or replace file
# >> append to file
grep -B 2 root < /etc/passwd | less
```

![Pipe between commands](./PRACTICAL1-UNIX/images/command-grepless.svg) <!-- .element: width="20%"-->

--

### RTFM: Bash redirections

[Bash redirections](https://www.gnu.org/software/bash/manual/html_node/Redirections.html)

---

## Regular expressions: Regex

> In computing, a regular expression is a specific pattern that provides concise and flexible means to "match" (specify and recognize) strings of text, such as particular characters, words, or patterns of characters.
> 
> Common abbreviations for "regular expression" include regex and regexp.
> - [Regular expression](http://en.wikipedia.org/wiki/Regular_expression)

--

### Graphical representation

A regular expression can be represented by an *automata*

![Automata](./PRACTICAL1-UNIX/images/automata.svg) <!-- .element: width="50%"-->

--

### Occurrence of a regular pattern

If one can get to the final state, the text `match` the regular expression.


![tot*o](./PRACTICAL1-UNIX/images/exp1.svg) <!-- .element: width="50%"-->

> Tutu et **_toto_** sont sur un bateau. Toto tombe à l’eau.

> Obama: «If daughters get tat**_too_**s, we will **_too_**»

--

### Exemples of regular expressions

Regular expressions defined on DNA &rarr; &Sigma;={A,C,G,T}

| Regular expression            | Automata                                                            |
|-------------------------------|---------------------------------------------------------------------|
| `ATG` (start codon)           | ![ATG](./PRACTICAL1-UNIX/images/exp2.svg) <!-- .element: width="100%"--> |
| `[ATG]TG` <br> `[^C]TG`(all start codons)  | ![[ATG]TG](./PRACTICAL1-UNIX/images/exp3.svg) <!-- .element: width="100%"--> |

--

### Exemples of regular expressions

Regular expressions defined on DNA &rarr; &Sigma;={A,C,G,T}

| Regular expression            | Automata                                                            |
|-------------------------------|---------------------------------------------------------------------|
| `.TG` <br> `[ACGT]TG` (all codons ending with TG) | ![.TG](./PRACTICAL1-UNIX/images/exp4.svg) <!-- .element: width="100%"--> |
| `TTA+TT` <br> `TTAA*TT` (TT, at least one A, TT)  | ![TTAT+TT](./PRACTICAL1-UNIX/images/exp5.svg) <!-- .element: width="100%"--> |

--

### Exemples of regular expressions

Regular expressions defined on DNA &rarr; &Sigma;={A,C,G,T}

| Regular expression            | Automata                                                            |
|-------------------------------|---------------------------------------------------------------------|
| `TAA`&#124;`TAG`&#124;`TGA` <br> `T(AA`&#124;`AG`&#124;`GA)` (All stop codons) | ![All stops](./PRACTICAL1-UNIX/images/exp6.svg) <!-- .element: width="100%"--> |

--

### Syntax of regular expressions

| Syntax            | What it matches                                  |
|-------------------|--------------------------------------------------|
| `^`               | begining of the line                             |
| `$`               | end of the line                                  |
| `[]`              | set of characters                                |
| `[^]`             | all characters but these ones (ex: `TTA{3,5}TT`) |
| &#124;            | multiple choices                                 |
| `{}`              | repetitions                                      |
| `*`               | any number of times                              |
| `+`               | at least once                                    |
| `?`               | none or once                                     |
| `\*`              | the `*` character (same for `+`, `(`, `[`, ...)  |

--

### Special characters: Regular expression extensions

| Special characters| What it matches                                  |
|-------------------|--------------------------------------------------|
| `()`              | used to define sub-expressions                   |
| `\n`              | used to reuse the text matched by the `n`th sub-expression               |

--

### Syntax of extended regular expressions

Extended regular expressions defined on DNA &rarr; &Sigma;={A,C,G,T}

| Regular expression            | Automata                                                            |
|-------------------------------|---------------------------------------------------------------------|
| `([ACGT]{3})\1{9,}` (matching a strech of at least the same codon 10 times) | As the langage described is not regular, no automaton can be used to represent the expression |


[What is my regular expression engine capable of ?](https://en.wikipedia.org/wiki/Comparison_of_regular_expression_engines)

---

## The find utility

```
FIND(1)                                                             General Commands Manual                                                            FIND(1)

NAME
       find - search for files in a directory hierarchy

SYNOPSIS
       find [-H] [-L] [-P] [-D debugopts] [-Olevel] [starting-point...] [expression]

DESCRIPTION
       This  manual  page documents the GNU version of find.  GNU find searches the directory tree rooted at each given starting-point by evaluating the given
       expression from left to right, according to the rules of precedence (see section OPERATORS), until the outcome is known (the left hand  side  is  false
       for and operations, true for or), at which point find moves on to the next file name.  If no starting-point is specified, `.' is assumed.

       If  you  are using find in an environment where security is important (for example if you are using it to search directories that are writable by other
       users), you should read the `Security Considerations' chapter of the findutils documentation, which is called Finding Files and comes  with  findutils.
       That document also includes a lot more detail and discussion than this manual page, so you may find it a more useful source of information.

```

To get the list of options:
```
man find
```
 
--

### Some example usages of find

Be careful:
- files refers to files AND directories
- protect the `*` from being expanded by the shell by using the `'` characters.



Looking for all files in your home directory and its subdirectories ending with '.txt'
```
find ~ -name '*.txt'
```

Looking for all files in your home directory ending with '.txt'

```
find ~ -maxdepth 1 -name '*.txt'
```

Looking for all files in your home directory and its subdirectories modified since yesterday

```
find ~ -ctime 1
```

--


Looking for all files in your home directory and its subdirectories modified after the file 'fichier'

```
find ~ -cnewer fichier
```

Looking for empty files (only files, not directories) in your home directory and its subdirectories

```
find ~ -empty -type f
```

--

### Executing commands on the result of find

On can execute commands on the result of `find` by either:
- use the `-exec` parameter
- pipe the result of find into antother command (e.g. xargs)


Getting the 'long' info on text files in your home directory

```
find . -maxdepth 1 -name '*.txt' -exec ls -l {} \;
```
This executes for each file the command `ls -l file`


or


```
find . -maxdepth 1 -name '*.txt' | xargs ls -l 
```
this executes once the command `ls -l all_the_found_files`


---

## Basic bash scripting


One can use text files to store commands we will repeatedly apply and eventually with different parameters : This is scripting !

The basic skeleton of a correct script:

```
#!/bin/bash

set -u
set -e
set -x

my commands here
```

--

> In computing, a shebang is the character sequence consisting of the characters number sign and exclamation mark (#!) at the beginning of a script.
> [...]
> In Unix-like operating systems, when a text file with a shebang is used as if it is an executable, the program loader parses the rest of the file's initial line as an interpreter directive; the specified interpreter program is executed.
>
> - [Shebang](https://en.wikipedia.org/wiki/Shebang_(Unix))


`#!/bin/bash` means that the script should be interpreted as bash commands.

--

`set` is a bash command that allows to set default behaviour (applicable only for the bash interpreter started when launching the script):
- `set -u` Treat unset variables as an error when substituting
- `set -e` Exit immediately if a command exits with a non-zero status
- `set -x` Treat unset variables as an error when substituting



`set --help` for further details

--
### Bash variables

Creating variables:
```
var=val

#or

var="a b"
```
accessing the variable content:
```
$var
#or
${var}
```

setting a variable with the result of a command:

```
val=`ls`
#or
val=$(ls)
```

a usefull variable is `$$` that is always set and is the PID of the shell (i.e. a unique number very usefull to create temporary files !).

--

### Script parameters

Positional parameters are assigned via command line arguments and are accessible within a script as `$1`, `$2`...`$N` variables.

myScript.sh:
```
#!/bin/bash

f=$1

echo $f
```

```
>./myScript.sh tutu toto
tutu
>./myScript.sh 'tutu toto'
tutu toto
```

--

### Launching several commands in background and waiting for them to finish

(aka poor man's [paralell](https://www.gnu.org/software/parallel/))

```
#!/bin/bash

command1 &
command2 &

wait

command3
```
1. Execute `command1` and `command2` in parallel,
2. `wait` for the subcommands (`command1` and `command2`) to finish
3. Execute command3

--

### Usefull cheatsheet

https://devhints.io/bash

Describing:
- arrays
- loops
- conditional execution
- variable expansions
- ...

---


## The bash command challenge !


![CMDChallenge](./PRACTICAL1-UNIX/images/challenge.png) <!-- .element: width="80%"-->


[I accept the challenge !](https://cmdchallenge.com/)

